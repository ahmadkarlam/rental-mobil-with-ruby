class Customer
	attr_accessor :id, :name, :address

	def id=(id)
		@id = id
	end

	def name=(name)
		@name = name
	end

	def address=(address)
		@address = address
	end

	def id
		@id
	end

	def name
		@name
	end

	def address
		@address
	end
end