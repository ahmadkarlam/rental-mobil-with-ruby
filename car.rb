class Car
	attr_accessor :name, :stock

	def initialize(name, stock)
		@name = name
		@stock = stock
	end

	def name
		@name
	end

	def stock
		@stock
	end

	def name=(name)
		@name = name
	end

	def stock=(stock)
		@stock = stock
	end

	def show_stok_with_name
		puts "#{@name} #{tab}: #{@stock}"
	end
end