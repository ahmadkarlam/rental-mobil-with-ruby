require './helpers'
require './customer'
require './car'

class Menu
	attr_accessor :customers, :cars

	def initialize
		@customers = {}
		@cars = {
			"Alphard" => Car.new("Alphard", 10),
			"Avanza" => Car.new("Avanza", 20),
			"Yaris" => Car.new("Yaris", 5),
		}
	end

	def main
		menu = 0
		while (menu != 5)
			Helper.clear_screen()
			show_list_menu()
			menu = ask("Pilih menu ? ", Integer) { |q| q.in = 1..5 }
			case menu
			when 1
				Helper.clear_screen()
				input_pendaftaran()
			when 2
				Helper.clear_screen()
				show_stock_cars()
			end
		end
	end

	def input_pendaftaran
		customer = Customer.new
		puts "Pendaftaran"
		puts "================="
		customer.id = ask("Masukkan id \t\t: ")
		while @customers.has_key?(customer.id)
			puts "Id sudah ada yang punya"
			customer.id = ask("Masukkan id \t\t: ")
		end

		customer.name = ask("Masukkan nama \t\t: ")
		customer.address = ask("Masukkan alamat \t: ")

		@customers.store(customer.id, customer);
		puts "\n\nTekan sembaran untuk melanjutkan"
		gets
	end

	def show_stock_cars
		puts "Stock Mobil"
		puts "==================================="
		@cars["Alphard"].show_stok_with_name
		@cars["Avanza"].show_stok_with_name
		@cars["Yaris"].show_stok_with_name

		puts "\n\nTekan sembaran untuk melanjutkan"
		gets
	end

	def show_list_menu
		puts "Menu pilihan"
		puts "======================"
		puts "1 . Pendaftaran"
		puts "2 . Stok Mobil"
		puts "3 . Penyewaan"
		puts "4 . Pengembalian"
		puts "5 . Keluar"
	end
end